extends Area2D

var norantza = 1
const GRABITATEA = 10
const SPEED = 1500
var velocity = Vector2()

func norantza_aldatu (nora):
    norantza = nora
    if norantza == 1:
        $Sprite.flip_h = false
    else:
        $Sprite.flip_h = true

func _physics_process(delta):
    velocity.x = SPEED * delta * norantza
    translate(velocity)
    velocity.y += 0.001

func _on_Area2D_body_entered(body):
    if "etsaia" in body.name:
        body.hil()
        queue_free()