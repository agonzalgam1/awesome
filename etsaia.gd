extends KinematicBody2D

const GRABITATEA = 10

export (int) var speed = 500

var norantza = 1
var velocity = Vector2()

func hil():
    speed = 0
    $AnimatedSprite.play("hil")
    $Timer.start()

func _physics_process(delta):
    velocity.y += GRABITATEA
    velocity.x = speed * norantza
    move_and_slide(velocity,Vector2(0, -1))
    
    if is_on_wall():
        norantza = norantza * -1
    if norantza == 1:
        $AnimatedSprite.flip_h = true
    else:
        $AnimatedSprite.flip_h = false

func _on_Timer_timeout():
    $CollisionShape2D.disabled=true
    queue_free()