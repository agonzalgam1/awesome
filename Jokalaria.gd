extends KinematicBody2D

const GRABITATEA = 10
const BALA = preload("res://bala.tscn")

export (int) var salto= -300
export (int) var speed = 350

var norantza = 1
var velocity = Vector2()
var egoera = "geldi"

func get_input():
    velocity.x = 0
    if Input.is_action_pressed('right'):
        velocity.x = speed
        $AnimatedSprite.flip_h = false
        norantza = 1
    if Input.is_action_pressed('left'):
        velocity.x = -speed
        $AnimatedSprite.flip_h = true
        norantza = -1
    if Input. is_action_just_pressed("fire"):
        var bala = BALA.instance()
        get_parent().add_child(bala)
        bala.position = $Position2D.global_position
        bala.norantza_aldatu(norantza)
        print("bala:",bala.position)
        print("jok:",$Position2D.global_position)
    if Input.is_action_pressed('up') and is_on_floor():
        velocity.y = salto

func _physics_process(delta):
    get_input()
    velocity.y += GRABITATEA
    if velocity.x != 0:
        $AnimatedSprite.play("ibili")
    else:
        $AnimatedSprite.play("geldi")
    if Input.is_action_pressed("down") and is_on_floor():
        $AnimatedSprite.play("behera")
    if Input.is_action_pressed('up'):
        $AnimatedSprite.play("gora")
    move_and_slide(velocity,Vector2(0, -1))